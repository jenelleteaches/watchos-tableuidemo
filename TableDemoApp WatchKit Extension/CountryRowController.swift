//
//  CountryRowController.swift
//  TableDemoApp WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit

class CountryRowController: NSObject {
    
    
    @IBOutlet weak var flagImage: WKInterfaceImage!
    
    @IBOutlet weak var countryNameLabel: WKInterfaceLabel!
    
}
